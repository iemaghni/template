Requirements:
  - ISO C++ 2014 compliant compiler and standard library:
    - GCC >= 5.0
    - LLVM >= 3.4
    - MSVC >= 2017 15.0
  - CMake
  - Ninja (or Make)
  - pkg-config
  - GLFW 3

Build instructions:
  - git clone https://<...>.git
  - cd <...>
  - mkdir build
  - cd build
  - cmake -G Ninja ..
  - ninja
  - ./main
